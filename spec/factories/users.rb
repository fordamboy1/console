# frozen_string_literal: true

# This will guess the User class
FactoryBot.define do
  factory :user do
    email { "username@example.com" }
    password { "password" }
    full_name { "John Doe" }
  end
end
