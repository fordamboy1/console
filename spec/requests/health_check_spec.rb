# frozen_string_literal: true

require "rails_helper"

describe "Healthcheck", type: :request do
  it "checks application health" do
    get "/healthcheck"

    expect(response.status).to eq(200)
  end
end
