class Users::CurrentUserController < ApplicationController
  skip_before_action :authenticate_user
  def show
  end

  def edit
    @user = current_user
    if @user.update!(current_user_params)
      render @user.as_json
    else
      render json: @user.errors.to_json
    end
  end

  private

  def current_user_params
    params.require(:user).permit(:avatar)
  end
end
