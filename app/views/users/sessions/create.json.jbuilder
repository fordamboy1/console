json.user do |json|
  json.partial! "users/user", user: current_user
  json.token user.generate_jwt
end
