json.call(user, :id, :email, :full_name)

json.avatar_url do
  user.avatar.attached? ? rails_blob_url(user.avatar, disposition: "attachment") : ""
end
