# frozen_string_literal: true

class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher
  validates :full_name, presence: true

  has_one_attached :avatar

  validates :avatar, content_type: ["image/png", "image/jpeg"],
    dimension: {width: {min: 200, max: 2400},
                height: {min: 200, max: 2400}}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
    :registerable,
    :recoverable,
    :rememberable,
    :validatable,
    :confirmable,
    :jwt_authenticatable,
    jwt_revocation_strategy: self
end
