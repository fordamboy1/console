# pull official base image
FROM ruby:3.1.2

# Install vips - required for rails attachments
RUN apt-get update -qq && apt-get install -y --no-install-recommends libvips42
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -

# set working directory
WORKDIR /app

# install app dependencies
COPY Gemfile ./
COPY Gemfile.lock ./
RUN bundle config set --local path 'vendor/cache'
RUN bundle install

# add app
COPY . ./

# start app
CMD ["./bin/rails", "server", "-b", "0.0.0.0"]
