# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.1.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0.3"

# Needs to be high enough to make sure the ENV is available for gem configuration
gem "dotenv-rails", groups: %i[development test]

# Use pg as the database for Active Record
gem "pg"

# Cross-domain requests
gem "rack-cors", "~> 0.4.0"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production
gem "redis"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

group :development, :test, :ci do
  gem "debug", platforms: %i[mri mingw x64_mingw]
  gem "listen"

  # Simple linting
  gem "standardrb"
  gem "mdl", "~> 0.11.0"
end

group :development do
  gem "web-console"
  gem "letter_opener"
end

group :test, :ci do
  gem "rspec-rails"
  gem "factory_bot_rails"
end

gem "active_storage_validations"
gem "avo"
gem "devise"
gem "devise-jwt"
gem "json_api_responders"
gem "rails-healthcheck"
gem "ruby-vips"
