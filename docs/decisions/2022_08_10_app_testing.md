# Our testing approach

## Context and Problem Statement

We want to provide a high-quality experience for our users and developers. To achieve that we need multiple layers of testing starting from low-level (methods, classes) up to manual QA (end-to-end testing).

## Considered Options

1. Rspec
2. Capybara
3. Cucumber
4. API contracts
5. Jest
6. Internal QA
7. External QA
8. Static code analysis
9. Testing environments
10. Feature flags
11. CI pipelines per branch
12. Canary deployments

## Decision Outcome

Due to our teams' experience with testing, we've decided to use the following:

1. Backend: rspec-based suite along with standard testing gems - rails-rspec, factory-bot, webmock and VCR
2. Frontend: Jest with extensions
3. API contracts: we will use postman for this (see decisions/2022_08_10_web_api_interface_documentation.md)
4. Static code analysis: standardrb, rails-best-practices, breakman, prettify, sonarcloud #TODO: install missing gems
5. QA to be done by the internal team for the foreseeable future
6. We will use local, staging, and production environments for end-to-end testing

Due to added complexity, we will skip the following options initially:

1. Feature flags for gradual rollout
2. CI pipelines for branch-specific testing
3. Canary deployments
