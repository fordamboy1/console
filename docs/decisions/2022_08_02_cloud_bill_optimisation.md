# Limit cloud bills

## Context and Problem Statement

How can we make sure we're cashflow positive on each user?

Read the [pricing issue](https://gitlab.com/synura/synura.com/-/merge_requests/7) for more context
Use [google sheet](https://docs.google.com/spreadsheets/u/1/d/1XpdFvSMHAkvH_lAXuoQQyrZPyHx7y2WgEWKcVlWYbgA/edit#gid=0) to play with the numbers

## Considered Options

1. Use highest GCP tier storage and don't try to limit bandwidth
2. Use regular hosting provider for storage (eg. hetzner)
3. Optimise storage and bandwidth within GCP and our app
4. Optimise codec's selection

## Decision Outcome

Chosen option: "Optimise storage and bandwidth" and "Enforce AV1 codec usage where possible"

1. To save on storage cost we will use `archive` storage tier by default and only temporarily switch to `standard` storage when user(s) show activity in a project (via async controller hook in in project/show request). We will also remove a video from `standard` after 30 minutes of inactivity
2. To save on bandwidth cost we will stream the actively edited video in HD or lower
3. We will show 4k+ video only when the project moves to a `publishing` stage
4. We will enforce AV1 codec usage where possible. We will test encoders from GCP and Bitmovin to see what works best. We will also need to use a player that can self optimise codecs, bitrate and resolution (eg. Bitmovin player)
5. We will track actual usage against licenses in our admin dashboard so that we can validate our assumptions of user behavior
6. We also plan to not release our product in other locales (particularly Asia) where the egress cost is more expensive, at least until we understand our cost, user behavior, and margins better.
