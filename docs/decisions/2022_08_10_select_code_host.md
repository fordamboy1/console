# Select code provider

## Context and Problem Statement

We should choose a code host to use for our product. Options are being validated based on the following criteria:

1. Ease of use and team familiarity
2. Availability of CI
3. High-quality issue tracking
4. Ease of source availability for contributors

## Considered Options

1. GitLab is something the team is familiar with, especially CI (Jason was product director for CI at GitLab).
   It also has much better issue tracking and epics compared to GitHub, which are quite poor and need add-ons, which even then don't integrate well.
2. GitHub is a good choice also, but the CI is a bit more finicky and as mentioned above issue quality is a bit poor.
   The one major advantage it has is that it is the primary platform for open source discovery.

## Decision Outcome

Given the tradeoffs above, we have chosen GitLab because:

1. The issues/epics are significantly better.
2. Although GitHub is better for open source discovery, that's not a primary discovery path for our product.

CI, although better at GitLab in terms of team experience, was not important enough on its own to contribute to the decision.
