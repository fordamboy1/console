# frozen_string_literal: true

Rails.application.routes.draw do
  root to: "healthcheck/healthchecks#check", format: :json

  defaults format: :json do
    Healthcheck.routes(self)
    devise_for :users, controllers: {sessions: "users/sessions",
                                     registrations: "users/registrations",
                                     passwords: "users/passwords"}

    get "current_user", to: "users/current_user#show"
    put "current_user", to: "users/current_user#edit"
  end

  mount Avo::Engine, at: Avo.configuration.root_path
end
