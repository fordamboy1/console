Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins ENV.fetch("FRONTEND_URI")
    resource "*", headers: :any, methods: :any, expose: ["Authorization"]
  end
end
